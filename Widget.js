/*global define, console, setTimeout*/
define([
    //dojo
    "dojo/_base/declare",
    "jimu/BaseWidget",
    "dijit/_WidgetsInTemplateMixin",
    "dijit/form/Button",
    "dojo/on",
    "dojo/aspect",
    "dijit/form/SimpleTextarea",
    "dojo/dom-class",
    "dojo/_base/array",
    "dojo/string",
    "dojo/_base/lang",
    "dojo/Deferred",
    'dojo/promise/all',

    //esri
    'jimu/WidgetManager',
    'jimu/PanelManager',
    'esri/config',
    "esri/InfoTemplate",
    "esri/geometry/Point",
    "esri/geometry/Extent",
    "esri/tasks/query",
    "esri/tasks/QueryTask",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/layers/FeatureLayer",
    "esri/InfoTemplate",
    'dojo/promise/all',
    "dojo/dom-class",
    'esri/lang',
    'jimu/dijit/LoadingShelter',
    "esri/request",
    "esri/Color",
    "dojo/domReady!",
    "dijit/registry"
  ],
  function (
    //dojo
    declare, BaseWidget, _WidgetsInTemplateMixin, Button, on, aspect, SimpleTextarea,
    domClass, array, dojoString, lang, Deferred, all,

    //esri
    WidgetManager, PanelManager, esriConfig, InfoTemplate, Point, Extent, Query, QueryTask,
    SimpleLineSymbol, SimpleFillSymbol, SimpleMarkerSymbol, FeatureLayer, esriRequest, InfoTemplate, all,
    domClass, esriLang, LoadingShelter, Color, registry, arrayUtils
  ) {
    //To create a widget, you need to derive from BaseWidget.
    return declare([BaseWidget, _WidgetsInTemplateMixin], {
      // DemoWidget code goes here

      //please note that this property is be set by the framework when widget is loaded.
      //templateString: template,

      baseClass: 'jimu-widget-PickColumns',
      name: "PickColumns",
      label: "PickColumns",

      layerQuery: null,
      serviceQuery: null,
      optionFieldSel: null,
      prevFieldNode: null,
      curWidget: null,
      panelManager: null,
      widgetManager: null,
      layersVisible: null, //0 ~ all, 1 ~ 2013, 2 ~ 2014, 3 ~ 2015 ..... 
      query: null,
      baseWhere: null,
      yearWhere: null,
      queryTask: null,
      currTab: null, // 0 (first menu on top), 1 (2nd menu from top), 2, 3
      currSet: null,
      currField: null,
      maxF: null,
      minF: null,
      prox: null,
      yearArr: null,
      yearArrI: null,
      featureLayer: null,


      postCreate: function () {
        this.inherited(arguments);
        console.log('postCreate');
      },

      // startup: function () {
      //   this.inherited(arguments);
      //   this.mapIdNode.innerHTML = 'map id:' + this.map.id;
      //   console.log('startup');
      // },

      startup: function startupFunc() {
        this.inherited(arguments);

        this.own(on(this.selectGraph, "change", lang.hitch(this, function () {
          if (this.selectGraph.get('value') != "Choose one:") {
            this.curTab = 0;
            this.selectAVG.set('value', "Choose one:");
            // this.selectMIN.set('value', "Choose one:");
            // this.selectMAX.set('value', "Choose one:");
            this.optionLayerSelVal = this.selectGraph.get('value');
            this.optionLayerSelText = this.selectGraph.get('displayedValue');
            this.openGraph(this.selectGraph.get('value'));
          }
        })));

        this.own(on(this.selectAVG, "change", lang.hitch(this, function () {
          if (this.selectAVG.get('value') != "Choose one:") {
            this.curTab = 1;
            this.selectGraph.set('value', "Choose one:");
            //this.selectMIN.set('value', "Choose one:");
            //this.selectMAX.set('value', "Choose one:");
            this.optionLayerSelVal = this.selectAVG.get('value');
            this.optionLayerSelText = this.selectAVG.get('displayedValue');
            this.openGraph(this.selectAVG.get('value'), 0);
          }
        })));

        // this.own(on(this.selectMIN, "change", lang.hitch(this, function () {
        //   if (this.selectMIN.get('value') != "Choose one:") {
        //     this.curTab = 2;
        //     this.selectGraph.set('value', "Choose one:");
        //     this.selectAVG.set('value', "Choose one:");
        //     this.selectMAX.set('value', "Choose one:");
        //     this.optionLayerSelVal = this.selectMIN.get('value');
        //     this.optionLayerSelText = this.selectMIN.get('displayedValue');
        //     this.openGraph(this.selectMIN.get('value'), 0);
        //   }

        // })));

        // this.own(on(this.selectMAX, "change", lang.hitch(this, function () {
        //   if (this.selectMAX.get('value') != "Choose one:") {
        //     this.curTab = 3;
        //     this.selectGraph.set('value', "Choose one:");
        //     this.selectAVG.set('value', "Choose one:");
        //     this.selectMIN.set('value', "Choose one:");
        //     this.optionLayerSelVal = this.selectMAX.get('value');
        //     this.optionLayerSelText = this.selectMAX.get('displayedValue');
        //     this.openGraph(this.selectMAX.get('value'), 0);
        //   }
        // })));
        this.fetchDataByName('Splash');
        this.drawSwitches();
      },

      getVisibleLayers: function () {

        // this.map._layers is an array. But because the basemap has these layers hidden, even when making
        // new layers visible, _layers.length = 0. So I check if the maps I want are null, and if not, assume loaded.\

        this.layersVisible = ["none", "none", "none", "none", "none", "none", "none", "none"]; //0 ~ all, 1 ~ 2013, 2 ~ 2014, 3 ~ 2015 .....
        var num = 0;

        if (this.map._layers.IPMP_DBO_Split_by_Date_7316 != null)
          if (this.map._layers.IPMP_DBO_Split_by_Date_7316.visible != false) { // 2013
            this.layersVisible[0] = "IPMP_DBO_Split_by_Date_7316";
            num++
          }

        if (this.map._layers.IPMP_DBO_Split_by_Date_4972 != null) // 2014
          if (this.map._layers.IPMP_DBO_Split_by_Date_4972.visible != false) {
            this.layersVisible[1] = "IPMP_DBO_Split_by_Date_4972";
            num++
          }
        if (this.map._layers.IPMP_DBO_Split_by_Date_33 != null) // 2015
          if (this.map._layers.IPMP_DBO_Split_by_Date_33.visible != false) {
            this.layersVisible[2] = "IPMP_DBO_Split_by_Date_33";
            num++
          }
        if (this.map._layers.IPMP_DBO_Split_by_Date_2166 != null) //2016
          if (this.map._layers.IPMP_DBO_Split_by_Date_2166.visible != false) {
            this.layersVisible[3] = "IPMP_DBO_Split_by_Date_2166";
            num++
          }
        if (this.map._layers.IPMP_DBO_Split_by_Date_3815 != null) // 2017
          if (this.map._layers.IPMP_DBO_Split_by_Date_3815.visible != false) {
            this.layersVisible[4] = "IPMP_DBO_Split_by_Date_3815";
            num++
          }
        if (this.map._layers.IPMP_DBO_Split_by_Date_394 != null) // 2018
          if (this.map._layers.IPMP_DBO_Split_by_Date_394.visible != false) {
            this.layersVisible[5] = "IPMP_DBO_Split_by_Date_394";
            num++
          }
        // if (this.map._layers.IPMP_DBO_2019_ != null.visible != false)
        //   this.layersVisible[7] = "IPMP_DBO_2019_";
        // if (this.map._layers.IPMP_Roads_Subset_9244 != null.visible != false)
        //   this.layersVisible[8] = "IPMP_Roads_Subset_9244";
        return num;
      },

      // AP - Store data in Data Manager for access by other widgets
      publish: function () {
        // this.publishData({
        //   value: this.chooseFieldNode.get('value')
        // }, true);
        // console.log("Published data to data manager: " + this.chooseFieldNode.get('value'));
      },

      openGraph: function (newFieldNode) {

        if (newFieldNode == this.prevFieldNode)
          return;
        console.log("new field node was " + newFieldNode);

        let widConf;
        this.query.where = this.baseWhere;
        this.query.outFields = ['*']; // just in case
        this.curField = null;

        switch (newFieldNode) {
          // Menu 1
          case 'Averages by Jurisdiction':
            widConf = this._getWidgetConfig('Averages by Jurisdiction', false);
            break;
          case 'RPA in Current Extent':
            widConf = this._getWidgetConfig('RPA in Current Extent', false);
            break;

            // Menu 2
          case 'ALLIG':
            widConf = this._getWidgetConfig('ALLIG', false);
            break;
          case 'AVG_FLT':
            widConf = this._getWidgetConfig('AVG_FLT', false);
            break;
          case 'FAILURE':
            widConf = this._getWidgetConfig('FAILURE', false);
            break;
          case 'FAIL_COUNT':
            widConf = this._getWidgetConfig('AVG_FLT', false);
            break;
          case 'IRI':
            widConf = this._getWidgetConfig('IRI', false);
            break;
          case 'LONG':
            widConf = this._getWidgetConfig('LONG', false);
            break;
          case 'LONG_NP':
            widConf = this._getWidgetConfig('LONG_NP', false);
            break;
          case 'LONG_WP':
            widConf = this._getWidgetConfig('LONG_WP', false);
            break;
          case 'PATCH_G':
            widConf = this._getWidgetConfig('PATCH_G', false);
            break;
          case 'RUT':
            widConf = this._getWidgetConfig('RUT', false);
            break;
          case 'TRANS':
            widConf = this._getWidgetConfig('TRANS', false);
            break;
          default:
            console.log("newFieldNode matched no case! this.curField node was " + newFieldNode);
            widConf = null;
            break;
        }

        if (this.curTab != 0) {
          this.curField = newFieldNode;
          this.query.outFields = [this.curField];
          this.query.where += ` AND ${this.curField}<>0`;
        }
        this.runYearQuery();

        if (widConf == null)
          return;

        var headWidget = WidgetManager.getInstance().getControllerWidgets()[0]; // WidgetManager.getInstance().getWidgetByLabel(this._getWidgetConfig('Header', true).label);
        widConf.visible = true;
        //This actually opens the widget 
        headWidget._createItem(widConf, headWidget.iconList.length + 1);
        headWidget.setOpenedIds([widConf.id]);
        // WidgetManager.getInstance().getControllerWidgets()[0].setOpenedIds([widConf.id]);

        console.log("!!__ The dataSource.name is: " + widConf.config.dataSource.name);
        console.log("!!__ The dataSource.layerId is: " + widConf.config.dataSource.layerId);

        if (this.curWidget != null) {
          PanelManager.getInstance().closePanel(this.curWidget.id + '_panel');
          headWidget._removeFromOpenedIds([this.curWidget.id]);
          //this.curWidget = null;
        }
        this.curWidget = widConf;
        prevFieldNode = newFieldNode;
        headWidget.resize();
      },

      //utility function to get the proper widget config based on the widget name !Not Label but Name!  
      _getWidgetConfig: function (widget, useName) {
        var widgetCnfg = null;
        array.some(WidgetManager.getInstance().appConfig.widgetPool.widgets, function (aWidget) {
          if ((aWidget.label == widget && !useName) || (aWidget.name == widget && useName)) {
            widgetCnfg = aWidget;
            return true;
          }
          return false;
        });
        if (!widgetCnfg) {
          /*Check OnScreen widgets if not found in widgetPool*/
          array.some(WidgetManager.getInstance().appConfig.widgetOnScreen.widgets, function (aWidget) {
            if ((aWidget.label == widget && !useName) || (aWidget.name == widget && useName)) {
              widgetCnfg = aWidget;
              return true;
            }
            return false;
          });
        }
        return widgetCnfg;
      },

      drawSwitches: function () {
        for (let i = 0; i < window.yearArrI.length; i++) {
          if (window.yearArrI[i]) {
            // add / make switches visible
            var label = document.createElement('label');
            label.setAttribute('class', 'switch');

            var div = document.createElement('div');
            div.setAttribute('class', 'slider' + i + ' round'); // to change is the div class

            label.appendChild(window.yearArr[i]);
            label.appendChild(div);
            document.getElementById("PickColumns__32").insertBefore(label, document.getElementById("yearBrk"));
          }
        }
      },

      onReceiveData: function (name, widgetId, data, historyData) {
        if (name == "Splash") {
          //filter out messages
          this.query = historyData[historyData.length - 2].value; // I'm assuming nothing else is using the data manager and know that query is at index 0
          this.query.returnDistinctValues = false;
          this.query.returnGeometry = true;
          this.baseWhere = this.query.where;
          this.queryTask = new QueryTask(historyData[historyData.length - 1].value);

          // Show PCI and PCI6 if RPA, only PCI6 if jurisdiction of city, and only PCI if County and not RPA 
          var str = this.query.where;
          var index = str.indexOf('=');
          if (str.substring(0, index) != 'RPA') {
            if (str.length > index + 4 && str.substring(index + 2, index + 4) == 'CI') {
              this.selectAVG.removeOption('PCI');
            } else {
              this.selectAVG.removeOption('PCI6');
            }
          }
          console.log("Received data from " + name + ". Query.where is: " + this.query.where);
        } else if (name == "selectOR") {
          var temp = [];
          // this.updateSource(temp.filter(n => !data.includes(n.attributes.OBJECTID)), 0);
          for (let i = 0; i < this.curSet.features.length; i++) {
            if (data.includes(this.curSet.features[i].attributes.OBJECTID))
              temp.push(this.curSet.features[i]);
          }
          this.updateSource(this.curSet, temp);
        } else if (name == "selectCLR") {
          this.updateSource(this.curSet, this.curSet.features);
        } else if (name == "yearToggle") {
          this.runYearQuery();
        } else {

        }
      },

      runYearQuery: function () {

        // if (this.yearWhere != null)
        //   this.query.where += this.yearWhere;

        let year, tmp;
        for (let i = 0; i < window.yearArrI.length; i++) {
          if (window.yearArrI[i] == 2) {
            if (year != null) {
              year += ' OR YEAR = ' + (2013 + i);
            } else {
              year = ' AND (YEAR = ' + (2013 + i);
            }
          }
        }
        tmp = this.query.where;
        if (year == null) {
          this.yearWhere = this.query.where;
          this.query.where += ' AND (YEAR = 1000)'; // return no results
        } else {
          year += ')';
          this.yearWhere = year;
          this.query.where += year;
        }

        this.queryTask.execute(this.query, result => this.updateSource(result, 0));
        console.log("Executed query with where = " + this.query.where);
        this.query.where = tmp;
      },

      updateSource: function (result, isSel) {

        if (!isSel)
          isSel = result.features;

        try {
          this.updateDataSourceData(0, {
            features: isSel
          });
          this.setIndicatorLayer();
        } catch (e) {
          console.error(e);
        }

        this.curSet = result;

        if (this.curField) {
          this.setMinandMax(isSel);
          this.setIndicatorLayer(isSel);
        }
      },

      setMinandMax: function (thisSet) {
        if (!this.curField)
          return;
        var arr = [];
        for (let i = 0; i < thisSet.length; i++)
          arr.push(thisSet[i].attributes[this.curField]);
        this.maxF = Math.max.apply(Math, arr);
        this.minF = Math.min.apply(Math, arr);
        console.log(`For ${this.curField}, max value  was ${this.maxF} & min was ${this.minF}`);
      },

      setIndicatorLayer: function (theSet) {
        console.log("Drawing indicator layer");
        if (this.query.returnGeometry == false)
          return;
        if (this.featureLayer) {
          this.map.removeLayer(this.featureLayer);
          this.featureLayer = null;
        }

        if (this.curSet.length === 0) {
          alert('No results found.');
        } else {

          var geometryTypeQuery = this.curSet.features.geometryType;

          var symbolQuery = new SimpleLineSymbol(
            SimpleLineSymbol.STYLE_SOLID,
            new Color(([128, 128, 128]), 1),
            2);

          //create a feature collection
          var featureCollection = {
            "layerDefinition": null,
            "featureSet": {
              "features": [],
              "geometryType": geometryTypeQuery
            }
          };

          featureCollection.layerDefinition = {
            "geometryType": geometryTypeQuery,
            "drawingInfo": {
              "renderer": {
                "type": "simple",
                "symbol": symbolQuery.toJson()
              }
            },
            "fields": this.curSet.fields
          };

          var infoTemplateQuery = new InfoTemplate("Details", "${*}");
          //create a feature layer based on the feature collection
          this.featureLayer = new FeatureLayer(featureCollection, {
            id: `${this.curField} Indicator`,
            infoTemplate: infoTemplateQuery
          });

          this.map.addLayer(this.featureLayer);

          //loop though the features
          for (var i = 0, len = theSet.length; i < len; i++) {
            var feature = theSet[i];
            diff = parseInt(128 - 128 * (feature.attributes[this.curField] / (this.maxF - this.minF)));
            feature.setSymbol(new SimpleLineSymbol( // not efficient to create new symbol for each item in table, but needed to add unique color
              SimpleLineSymbol.STYLE_SOLID,
              new Color(([255 - diff, Math.abs(0 - diff), Math.abs(0 - diff)]), 1), // low diff = high value relative to max
              3));
            this.featureLayer.add(feature);
          }
          this._openResultInAttributeTable(this.featureLayer);
        }
      },


      _listenDSManagerUpdateEvent: function () {
        this.exdsBeginUpdateHandle = on(this.dataSourceManager, 'begin-update',
          lang.hitch(this, function (dsid) {
            this._handleLoadingStatusForExds(dsid, true);
          }));
      },

      _checkDataSource: function (config) {
        var dataSource = config && config.dataSource;
        var res = this._checkDataSourceCode(dataSource);
        this._avalidDataSource = res.code === 0;
        if (!this._avalidDataSource && this.mainDijit) {
          this.mainDijit.showNodata(res.message);
        }
      },

      _openResultInAttributeTable: function (currentLayer) {
        if (this.autozoomtoresults) {
          setTimeout(lang.hitch(this, function () {
            this.zoomall();
          }), 300);
        }
        var aLayer = {
          layerObject: currentLayer,
          title: currentLayer.name,
          id: currentLayer.id,
          getLayerObject: function () {
            var def = new Deferred();
            if (this.layerObject) {
              def.resolve(this.layerObject);
            } else {
              def.reject("layerObject is null");
            }
            return def;
          }
        };
        if (!Object.create) {
          Object.create = function (proto, props) {
            if (typeof props !== "undefined") {
              throw "The multiple-argument version of Object.create is not provided by this browser and cannot be shimmed.";
            }

            function ctor() {}
            ctor.prototype = proto;
            return new ctor();
          };
        }
        this.publishData({
          'target': 'AttributeTable',
          'layer': Object.create(aLayer)
        });
      },

      onOpen: function () {
        console.log('onOpen');
      },

      onClose: function () {
        console.log('onClose');
      },

      onMinimize: function () {
        console.log('onMinimize');
      },

      onMaximize: function () {
        console.log('onMaximize');
      },

      onSignIn: function (credential) {
        /* jshint unused:false*/
        console.log('onSignIn');
      },

      onSignOut: function () {
        console.log('onSignOut');
      },
    });
  });